/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.placeholderview.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * ExpandableBean
 *
 * @param <T> 泛型
 * @since 2021-05-27
 */
public class ExpandableBean<T> {
    private String catergory;

    private boolean isSel;

    private int lastIndex = -1;

    private List<T> data = new ArrayList<>();

    /**
     * 构造函数
     */
    public ExpandableBean() {
    }

    /**
     * 构造函数
     *
     * @param catergory catergory
     * @param isSel     isSel
     * @param data      数据集合
     */
    public ExpandableBean(String catergory, boolean isSel, List<T> data) {
        this.catergory = catergory;
        this.isSel = isSel;
        this.data = data;
    }

    public boolean isSel() {
        return isSel;
    }

    public void setSel(boolean sel) {
        isSel = sel;
    }

    public String getCatergory() {
        return catergory;
    }

    public void setCatergory(String catergory) {
        this.catergory = catergory;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public int getLastIndex() {
        return lastIndex;
    }

    public void setLastIndex(int lastIndex) {
        this.lastIndex = lastIndex;
    }

    @Override
    public String toString() {
        return "ExpandableBean{"
                + "catergory='" + catergory + '\''
                + ", isSel=" + isSel
                + ", data=" + data
                + '}';
    }
}
