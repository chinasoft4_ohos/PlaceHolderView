/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.demo;

import ohos.agp.components.*;
import ohos.app.Context;

/**
 * CardPresenter
 *
 * @since 2021-05-27
 */
public class CardPresenter extends StackLayout {
    private Text question_caption, question_caption1;
    private Button btn_answer1, btn_answer2, btn_answer3;
    private String question;

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public CardPresenter(Context context) {
        this(context, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet 属性
     */
    public CardPresenter(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet 属性
     * @param styleName 样式名
     */
    public CardPresenter(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);

        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_card_layout, null, true);
        question_caption = (Text) component.findComponentById(ResourceTable.Id_question_caption);
        question_caption1 = (Text) component.findComponentById(ResourceTable.Id_question_caption1);
        btn_answer1 = (Button) component.findComponentById(ResourceTable.Id_btn_answer1);
        btn_answer2 = (Button) component.findComponentById(ResourceTable.Id_btn_answer2);
        btn_answer3 = (Button) component.findComponentById(ResourceTable.Id_btn_answer3);

        btn_answer1.setClickedListener(component13 -> {
            String message = String.format(
                    "The question is: %s.",
                    question);
            String message1 = String.format(
                    "The answer is: %s.",
                    "ANSWER1");
            question_caption.setText(message);
            question_caption1.setText(message1);
        });

        btn_answer2.setClickedListener(component12 -> {
            String message = String.format(
                    "The question is: %s.",
                    question);
            String message1 = String.format(
                    "The answer is: %s.",
                    "ANSWER2");
            question_caption.setText(message);
            question_caption1.setText(message1);
        });

        btn_answer3.setClickedListener(component1 -> {
            String message = String.format(
                    "The question is: %s.",
                    question);
            String message1 = String.format(
                    "The answer is: %s.",
                    "ANSWER3");
            question_caption.setText(message);
            question_caption1.setText(message1);
        });
        addComponent(component);
    }

    /**
     * 设置问题名称
     *
     * @param name 问题名称
     */
    public void setQuestionName(String name) {
        question = name;
        question_caption.setText(name);
    }
}
