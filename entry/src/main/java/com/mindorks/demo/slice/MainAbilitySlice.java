/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mindorks.demo.slice;

import com.mindorks.demo.*;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * MainAbilitySlice
 *
 * @since 2021-05-27
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private Button goSwipeDirectional;
    private Button goDemoLoadMore;
    private Button goSwipeTest;
    private Button goTinderTest;
    private Button goTinderSwipeInScroll;
    private Button goQuestion;
    private Button goPhotos;
    private Button goLoadMore;
    private Button goExpandable;
    private Button goFeedTest;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        goSwipeDirectional = (Button) findComponentById(ResourceTable.Id_goSwipeDirectional);
        goSwipeDirectional.setClickedListener(this);

        goDemoLoadMore = (Button) findComponentById(ResourceTable.Id_goDemoLoadMore);
        goDemoLoadMore.setClickedListener(this);

        goSwipeTest = (Button) findComponentById(ResourceTable.Id_goSwipeTest);
        goSwipeTest.setClickedListener(this);

        goTinderTest = (Button) findComponentById(ResourceTable.Id_goTinderTest);
        goTinderTest.setClickedListener(this);

        goTinderSwipeInScroll = (Button) findComponentById(ResourceTable.Id_goTinderSwipeInScroll);
        goTinderSwipeInScroll.setClickedListener(this);

        goQuestion = (Button) findComponentById(ResourceTable.Id_goQuestion);
        goQuestion.setClickedListener(this);

        goLoadMore = (Button) findComponentById(ResourceTable.Id_goLoadMore);
        goLoadMore.setClickedListener(this);

        goPhotos = (Button) findComponentById(ResourceTable.Id_goPhotos);
        goPhotos.setClickedListener(this);

        goExpandable = (Button) findComponentById(ResourceTable.Id_goExpandable);
        goExpandable.setClickedListener(this);

        goFeedTest = (Button) findComponentById(ResourceTable.Id_goFeedTest);
        goFeedTest.setClickedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_goSwipeDirectional:
                launchAbility(new AbilitySwipeDirectional());
                break;
            case ResourceTable.Id_goDemoLoadMore:
                launchAbility(new DemoLoadMoreAbility());
                break;
            case ResourceTable.Id_goSwipeTest:
                launchAbility(new AbilitySwipeTest());
                break;
            case ResourceTable.Id_goTinderTest:
                launchAbility(new AbilityTinder());
                break;
            case ResourceTable.Id_goTinderSwipeInScroll:
                launchAbility(new AbilityTinderSwipeInScroll());
                break;
            case ResourceTable.Id_goQuestion:
                launchAbility(new QuestionAbility());
                break;
            case ResourceTable.Id_goPhotos:
                launchAbility(new PhotosAbility());
                break;
            case ResourceTable.Id_goLoadMore:
                launchAbility(new LoadMoreAbility());
                break;
            case ResourceTable.Id_goExpandable:
                launchAbility(new ExpandableAbility());
                break;
            case ResourceTable.Id_goFeedTest:
                launchAbility(new FeedTestAbility());
                break;
            default:
                break;
        }
    }

    private void launchAbility(Ability ability) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withDeviceId("")
                .withAbilityName(ability.getClass().getName())
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }
}
