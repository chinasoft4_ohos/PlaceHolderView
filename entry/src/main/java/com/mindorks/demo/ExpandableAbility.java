/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.demo;

import com.mindorks.placeholderview.bean.ExpandableBean;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;

/**
 * ExpandableAbility
 *
 * @since 2021-05-27
 */
public class ExpandableAbility extends Ability implements Component.ClickedListener {
    private List<ExpandableBean> mExpandableList = new ArrayList<>();
    private Button mCollapseBtn;
    private Button mExpandBtn;
    private DirectionalLayout mExpandDireLayout;
    private List<String> mItemList;
    private List<Component> mComponentList = new ArrayList<>();
    private List<Image> mImageList = new ArrayList<>();
    private int mHeightInt = 0;
    private AnimatorValue animatorValue;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_expandable);

        initData();
        initView();
    }

    private void initData() {
        mExpandableList = setExpandData();
    }

    private void initView() {
        mCollapseBtn = (Button) findComponentById(ResourceTable.Id_collapse_random_btn);
        mCollapseBtn.setClickedListener(this);
        mExpandBtn = (Button) findComponentById(ResourceTable.Id_expand_random_btn);
        mExpandBtn.setClickedListener(this);

        mExpandDireLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_expand_dire_layout);
        for (int i = 0; i < mExpandableList.size(); i++) {
            Component cpt = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_item_expandable, null, false);
            final int ii = i;
            cpt.findComponentById(ResourceTable.Id_item_ex_icon).setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    setClickListener(cpt, ii);
                }
            });
            Text titleTex = ((Text) cpt.findComponentById(ResourceTable.Id_item_ex_title));
            titleTex.setText(mExpandableList.get(i).getCatergory());

            Image icon = (Image) cpt.findComponentById(ResourceTable.Id_item_ex_icon);
            DirectionalLayout exLayout = (DirectionalLayout) cpt.findComponentById(ResourceTable.Id_item_ex_layout);
            mItemList = mExpandableList.get(i).getData();
            for (int j = 0; j < mItemList.size(); j++) {
                Component feedCpt = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_item_ex, null, false);
                ((Text) feedCpt.findComponentById(ResourceTable.Id_ex_item_text)).setText(((String) mItemList.get(j)));
                feedCpt.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        setItemRemoveListener(ii, exLayout, component);
                    }
                });
                exLayout.addComponent(feedCpt);
            }
            mHeightInt = exLayout.getComponentAt(0).getHeight();
            mComponentList.add(cpt);
            mImageList.add(icon);
            mExpandDireLayout.addComponent(cpt);
        }
    }

    private List<ExpandableBean> setExpandData() {
        List<ExpandableBean> beanList = new ArrayList<>();
        beanList.add(new ExpandableBean("Apple", false, setExpandItem(1)));
        beanList.add(new ExpandableBean("Mango", false, setExpandItem(2)));
        beanList.add(new ExpandableBean("Orange", false, setExpandItem(3)));
        beanList.add(new ExpandableBean("Banana", false, setExpandItem(4)));
        beanList.add(new ExpandableBean("Papaya", false, setExpandItem(4)));
        beanList.add(new ExpandableBean("Papaya", false, setExpandItem(4)));
        beanList.add(new ExpandableBean("Papaya", false, setExpandItem(4)));
        beanList.add(new ExpandableBean("Papaya", false, setExpandItem(4)));
        return beanList;
    }

    private List<String> setExpandItem(int j) {
        List<String> itemList = new ArrayList<>();
        itemList.add(copyString("a", j));
        itemList.add(copyString("b", j));
        itemList.add(copyString("c", j));
        itemList.add(copyString("d", j));
        itemList.add(copyString("e", j));
        return itemList;
    }

    private String copyString(String s, int j) {
        StringBuffer stf = new StringBuffer();
        for (int i = 0; i < j; i++) {
            stf.append(s);
        }
        return stf.toString();
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_collapse_random_btn:
                if (mExpandableList.get(1).isSel()) {
                    setClickListener(mComponentList.get(1), 1);
                }
                break;
            case ResourceTable.Id_expand_random_btn:
                if (!mExpandableList.get(1).isSel()) {
                    setClickListener(mComponentList.get(1), 1);
                }
                break;
            default:
                break;
        }
    }

    private void setClickListener(Component cpt, int ii) {
        if (animatorValue != null && animatorValue.isRunning()){
            return;
        }
        int p = -1;
        for (int j = 0; j < mExpandableList.size(); j++) {
            if (mExpandableList.get(j).isSel()) {
                p = j;
            }
        }
        if (p != -1 && p != ii) {
            mExpandableList.get(p).setSel(false);
            mImageList.get(p).setPixelMap(ResourceTable.Media_ic_keyboard_arrow_up_white);
        }

        mExpandableList.get(ii).setSel(!mExpandableList.get(ii).isSel());

        DirectionalLayout feedLC = (DirectionalLayout) cpt.findComponentById(ResourceTable.Id_item_ex_layout);
        int feedHeight = 0;
        for (int j = 0; j < feedLC.getChildCount(); j++) {
            feedHeight += feedLC.getComponentAt(j).getHeight();
        }

        final int a = feedHeight;
        final int pp = p;
        animatorValue = new AnimatorValue();
        animatorValue.setDuration(60);
        animatorValue.setDelay(0);
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if (mExpandableList.get(ii).isSel()) {
                    feedLC.setHeight((int) (a * v));
                    if (pp != -1 && pp != ii) {
                        DirectionalLayout direLayout = (DirectionalLayout) mComponentList.get(pp).findComponentById(ResourceTable.Id_item_ex_layout);
                        direLayout.setHeight((int) (direLayout.getHeight() * (1 - v)));
                    }
                } else {
                    feedLC.setHeight((int) (a * (1 - v)));
                }
            }
        });
        animatorValue.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                if (mExpandableList.get(ii).isSel()) {
                    mImageList.get(ii).setPixelMap(ResourceTable.Media_ic_keyboard_arrow_down_white);
                } else {
                    mImageList.get(ii).setPixelMap(ResourceTable.Media_ic_keyboard_arrow_up_white);
                }
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {

            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animatorValue.start();
    }

    private void setItemRemoveListener(int ii, DirectionalLayout exLayout, Component component) {
        int jj = exLayout.getChildIndex(component);
        mExpandableList.get(ii).getData().remove(jj);
        exLayout.removeComponentAt(jj);
        if (exLayout.getHeight() >= mHeightInt) {
            exLayout.setHeight(exLayout.getHeight() - mHeightInt);
        }
    }
}
