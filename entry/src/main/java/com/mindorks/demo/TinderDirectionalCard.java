/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.demo;

import com.mindorks.placeholderview.BaseSwipeCard;
import ohos.agp.components.*;
import ohos.app.Context;

/**
 * TinderDirectionalCard
 *
 * @since 2021-05-27
 */
public class TinderDirectionalCard extends BaseSwipeCard {
    private Text nameText;
    private Component accept;
    private Component reject;

    @Override
    public void showAccept() {
        accept.setVisibility(VISIBLE);
        reject.setVisibility(HIDE);
    }

    @Override
    public void showReject() {
        accept.setVisibility(HIDE);
        reject.setVisibility(VISIBLE);
    }

    @Override
    public void hideAllTips() {
        accept.setVisibility(HIDE);
        reject.setVisibility(HIDE);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public TinderDirectionalCard(Context context) {
        this(context, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet 属性
     */
    public TinderDirectionalCard(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet 属性
     * @param styleName 样式名
     */
    public TinderDirectionalCard(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_tinder_card_view, null, true);
        nameText = (Text) component.findComponentById(ResourceTable.Id_nameAgeTxt);
        accept = component.findComponentById(ResourceTable.Id_tvAccept);
        reject = component.findComponentById(ResourceTable.Id_tvReject);
        addComponent(component);
    }

    /**
     * 设置姓名
     *
     * @param text 姓名集合
     */
    public void setNameText(String text) {
        nameText.setText(text);
    }

    public String getNameText() {
        return nameText.getText();
    }
}
