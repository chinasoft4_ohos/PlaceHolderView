/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.demo;

import com.mindorks.demo.provider.RowItemProvider;
import com.mindorks.placeholderview.LoadImageInstance;
import com.mindorks.placeholderview.Utils;
import com.mindorks.placeholderview.bean.ImageBean;
import com.squareup.picasso.Picasso;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.components.element.ShapeElement;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * LoadMoreAbility
 *
 * @since 2021-05-27
 */
public class LoadMoreAbility extends Ability {
    private DirectionalLayout mColumnImagesLayout;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_load_more);
        mColumnImagesLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_column_image_layout);
        List<ImageBean> imageBeanList = Utils.loadImages(this);

        for (int i = 0; i < 10; i++) {
            Image image = new Image(this);
            image.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                    500));

            Picasso.get()
                    .load(imageBeanList.get(i).getUrl())
                    .fit()
                    .tag(this)
                    .into(image);
            if (i != imageBeanList.size() - 1) {
                image.setMarginBottom(20);
            }
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setCornerRadiiArray(new float[]{20, 20, 20, 20, 20, 20, 20, 20});
            shapeElement.setShape(ShapeElement.RECTANGLE);
            shapeElement.setStroke(5, new RgbColor(211, 211, 211));
            image.setBackground(shapeElement);
            mColumnImagesLayout.addComponent(image);
        }
    }
}
