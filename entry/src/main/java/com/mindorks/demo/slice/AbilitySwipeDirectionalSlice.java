/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.demo.slice;

import com.mindorks.demo.ResourceTable;
import com.mindorks.demo.TinderDirectionalCard;
import com.mindorks.placeholderview.SwipeDirectionalView;
import com.mindorks.placeholderview.impl.ItemRemovedListener;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;

/**
 * AbilitySwipeDirectionalSlice
 *
 * @since 2021-05-27
 */
public class AbilitySwipeDirectionalSlice extends AbilitySlice implements Component.ClickedListener {
    private int mCount = 1;
    private Image rejectBtn, acceptBtn, undoBtn;
    private SwipeDirectionalView swd_view;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_tinder_directional_swipe);
        rejectBtn = (Image) findComponentById(ResourceTable.Id_rejectBtn);
        acceptBtn = (Image) findComponentById(ResourceTable.Id_acceptBtn);
        undoBtn = (Image) findComponentById(ResourceTable.Id_undoBtn);
        rejectBtn.setClickedListener(this);
        acceptBtn.setClickedListener(this);
        undoBtn.setClickedListener(this);

        swd_view = (SwipeDirectionalView) findComponentById(ResourceTable.Id_swd_view);
        swd_view.setItemRemovedListener(new ItemRemovedListener() {
            @Override
            public void onItemRemoved(int count) {
                if (count == 0) {
                    for (int i = 0; i < 10; i++) {
                        swd_view.addView(makeCard("Name " + mCount++));
                    }
                }
            }
        });

        for (int i = 0; i < 10; i++) {
            swd_view.addView(makeCard("Name " + mCount++));
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private TinderDirectionalCard makeCard(String cardName) {
        TinderDirectionalCard tinderDirectionalCard = new TinderDirectionalCard(getContext());
        tinderDirectionalCard.setNameText(cardName);
        return tinderDirectionalCard;
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_rejectBtn:
                swd_view.doSwipe(false);
                break;
            case ResourceTable.Id_acceptBtn:
                swd_view.doSwipe(true);
                break;
            case ResourceTable.Id_undoBtn:
                swd_view.undoLastSwipe();
                break;
            default:
                break;
        }
    }
}
