/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.placeholderview;

/**
 * SwipeDecor
 *
 * @since 2021-05-27
 */
public class SwipeDecor {
    private int paddingTop = 20;
    private float relativeScale = 0.02f;

    /**
     * 获取PaddingTop
     *
     * @return 数值
     */
    public int getPaddingTop() {
        return paddingTop;
    }

    /**
     * 设置PaddingTop
     *
     * @param paddingTop 边距
     * @return SwipeDecor
     */
    public SwipeDecor setPaddingTop(int paddingTop) {
        this.paddingTop = paddingTop;
        return this;
    }

    /**
     * 获取缩放数值
     *
     * @return 数值
     */
    public float getRelativeScale() {
        return relativeScale;
    }

    /**
     * 设置缩放数值
     *
     * @param relativeScale
     * @return SwipeDecor
     */
    public SwipeDecor setRelativeScale(float relativeScale) {
        this.relativeScale = relativeScale;
        return this;
    }
}
