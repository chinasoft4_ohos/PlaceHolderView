/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.demo;

import com.jcodecraeer.xrecyclerview.XRecyclerView;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.app.Context;

import java.util.List;

/**
 * MyAdapter
 *
 * @param <T> 泛型
 * @since 2021-05-27
 */
public class MyAdapter<T> extends XRecyclerView.RecyclerAdapter<T> {

    /**
     * 构造函数
     *
     * @param context   上下文
     * @param data      数据
     * @param mLayoutId 资源id
     */
    public MyAdapter(Context context, List data, int mLayoutId) {
        super(context, data, mLayoutId);
    }

    @Override
    public List getData() {
        return super.getData();
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public T getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer parent) {
        return super.getComponent(position, convertComponent, parent);
    }

    @Override
    public void setNumColumns(int numColumns) {
        super.setNumColumns(numColumns);
    }

    @Override
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        super.setOnItemClickListener(onItemClickListener);
    }

    @Override
    public void bind(ViewHolder holder, T s, int position) {
        InfiniteFeedInfo infiniteFeedInfo = (InfiniteFeedInfo) s;
        holder.<Text>getView(ResourceTable.Id_titleTxt).setText(infiniteFeedInfo.getTitle());
        holder.<Text>getView(ResourceTable.Id_captionTxt).setText(infiniteFeedInfo.getCaption());
        holder.<Text>getView(ResourceTable.Id_timeTxt).setText(infiniteFeedInfo.getTime());
    }
}
