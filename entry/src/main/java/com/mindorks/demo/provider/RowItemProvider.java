/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.demo.provider;

import com.mindorks.demo.ResourceTable;
import com.mindorks.placeholderview.LoadImageInstance;
import com.mindorks.placeholderview.bean.ImageBean;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * RowItemProvider
 *
 * @since 2021-05-27
 */
public class RowItemProvider extends BaseItemProvider {
    private Context mContext;
    private List<ImageBean> mImageBeanList = new ArrayList<>();

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param imageBeanList 数据集合
     */
    public RowItemProvider(Context context, List<ImageBean> imageBeanList) {
        mContext = context;
        mImageBeanList = imageBeanList;
    }

    @Override
    public int getCount() {
        return mImageBeanList.size();
    }

    @Override
    public Object getItem(int i) {
        if (i >= 0 && i < mImageBeanList.size()) {
            return mImageBeanList.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        if (component == null) {
            cpt = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_row, null, false);
        } else {
            cpt = component;
        }
        if (i != mImageBeanList.size() - 1){
            cpt.setMarginBottom(20);
        }
        Image imageLayout = (Image) cpt.findComponentById(ResourceTable.Id_url_image);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setCornerRadiiArray(new float[]{20, 20, 20, 20, 20, 20, 20, 20});
        shapeElement.setShape(ShapeElement.RECTANGLE);
        shapeElement.setStroke(5, new RgbColor(211,211,211));
        imageLayout.setBackground(shapeElement);

//        RoundedCorners roundedCorners = new RoundedCorners(20);
//        RequestOptions options = RequestOptions.bitmapTransform(roundedCorners);
//        Glide.with(mContext).load(mImageBeanList.get(i).getUrl())
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .skipMemoryCache(true)
//                .apply(options)
//                .into(imageLayout);

//        imageLayout.setPixelMap(mImageBeanList.get(i).getMediaId());
//        WeakReference<Image> imageWeakReference = new WeakReference<Image>(imageLayout);
//        mLoadImageInstance.loadImageFromUrlAsync(imageWeakReference, mImageBeanList.get(i).getUrl());
        return cpt;
    }
}
