/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.placeholderview;

import ohos.agp.render.Canvas;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.RectFloat;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

/**
 * ImageUtils
 *
 * @since 2021-05-27
 */
public class ImageUtils {
    /**
     * 圆形图片
     *
     * @param originMap PixelMap
     * @param width 宽
     * @param height 高
     * @return PixelMap
     */
    public static PixelMap setRoundImage(PixelMap originMap, int width, int height) {
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(width, height);
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.editable = true;
        // 创建结果PixelMap
        PixelMap circlePixelMap = PixelMap.create(options);
        Canvas canvas = new Canvas();
        // 将结果PixelMap作为画布背景
        Texture texture = new Texture(circlePixelMap);
        canvas.setTexture(texture);
        PixelMapHolder pixelMapHolder = new PixelMapHolder(PixelMap.create(originMap, options));
        RectFloat rectSrc = new RectFloat(0, 0, width, height);
        RectFloat rectDst = new RectFloat(0, 0, width, height);
        // 生成圆形PixelMap
        canvas.drawPixelMapHolderRoundRectShape(
                pixelMapHolder,
                rectSrc,
                rectDst,
                20,
                20
        );
        return circlePixelMap;
    }
}
