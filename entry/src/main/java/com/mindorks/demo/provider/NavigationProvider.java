/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.demo.provider;

import com.mindorks.demo.ResourceTable;
import com.mindorks.demo.bean.NavigationDataBean;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * NavigationProvider
 *
 * @since 2021-05-27
 */
public class NavigationProvider extends BaseItemProvider {
    private Context mContext;
    private List<NavigationDataBean> mNavigationList = new ArrayList<>();

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param navigationList 数据集合
     */
    public NavigationProvider(Context context, List<NavigationDataBean> navigationList) {
        mContext = context;
        mNavigationList = navigationList;
    }

    @Override
    public int getCount() {
        return mNavigationList.size();
    }

    @Override
    public Object getItem(int i) {
        if (i >= 0 && i < mNavigationList.size()) {
            return mNavigationList.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component cpt;
        if (component == null) {
            cpt = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_photos_navigation, null, false);
        } else {
            cpt = component;
        }

        ((Image) cpt.findComponentById(ResourceTable.Id_item_icon)).setPixelMap(mNavigationList.get(i).getIconId());
        ((Text) cpt.findComponentById(ResourceTable.Id_ex_item_text)).setText(mNavigationList.get(i).getName());
        return cpt;
    }
}
