/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.demo.slice;

import com.mindorks.demo.ResourceTable;
import com.mindorks.demo.TinderDirectionalCard;
import com.mindorks.placeholderview.SwipeDirectionalView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

/**
 * AbilityTinderSwipeInScrollSlice
 *
 * @since 2021-05-27
 */
public class AbilityTinderSwipeInScrollSlice extends AbilitySlice {
    private SwipeDirectionalView swd_view;
    private int mCount = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_tinder_swipe_in_scroll);

        swd_view = (SwipeDirectionalView) findComponentById(ResourceTable.Id_swipeView);
        swd_view.setSwipeType(SwipeDirectionalView.SWIPE_TYPE_HORIZONTAL);

        for (int i = 0; i < 10; i++) {
            swd_view.addView(makeCard("Name " + mCount++));
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private TinderDirectionalCard makeCard(String cardName) {
        TinderDirectionalCard tinderDirectionalCard = new TinderDirectionalCard(getContext());
        tinderDirectionalCard.setNameText(cardName);
        return tinderDirectionalCard;
    }
}
