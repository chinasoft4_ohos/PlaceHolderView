/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.placeholderview;

import ohos.agp.components.AttrSet;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

/**
 * BaseSwipeCard
 *
 * @since 2021-05-27
 */
public abstract class BaseSwipeCard extends StackLayout {
    /**
     * 展示接受
     */
    public abstract void showAccept();

    /**
     * 展示拒绝
     */
    public abstract void showReject();

    /**
     * 隐藏文字
     */
    public abstract void hideAllTips();

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public BaseSwipeCard(Context context) {
        this(context, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet 属性
     */
    public BaseSwipeCard(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet 属性
     * @param styleName 样式名
     */
    public BaseSwipeCard(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }
}
