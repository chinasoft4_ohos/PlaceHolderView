/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.placeholderview.impl;

import ohos.agp.components.Component;

/**
 * ICardState
 *
 * @since 2021-05-27
 */
public interface ICardState {
    /**
     * 移除卡片
     *
     * @param component 卡片
     * @param removeType 移除类型
     */
    void removeCardView(Component component, int removeType);

    /**
     * 接受卡片
     *
     * @param component 卡片
     */
    void acceptCard(Component component);

    /**
     * 不接受卡片
     *
     * @param component 卡片
     */
    void rejectCard(Component component);

    /**
     * 重置卡片
     *
     * @param component 卡片
     */
    void resetCard(Component component);
}
