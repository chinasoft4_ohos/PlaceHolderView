/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindorks.demo.bean;

/**
 * FeedDataBean
 *
 * @since 2021-05-27
 */
public class FeedDataBean {
    private String title;
    private String image_url;
    private String caption;
    private String time;

    /**
     * 构造函数
     */
    public FeedDataBean() {
    }

    /**
     * 构造函数
     *
     * @param title 标题
     * @param image_url 图片url
     * @param caption 内容
     * @param time 时间
     */
    public FeedDataBean(String title, String image_url, String caption, String time) {
        this.title = title;
        this.image_url = image_url;
        this.caption = caption;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "FeedDataBean{"
                + "title='" + title + '\''
                + ", image_url='" + image_url + '\''
                + ", caption='" + caption + '\''
                + ", time='" + time + '\''
                + '}';
    }
}
