
# PlaceHolderView

#### 项目介绍

- 项目名称：PlaceHolderView
- 所属系列：openharmony的第三方组件适配移植
- 功能：支持拖拽卡片、侧滑菜单、列表加载更多、横向滚动列表以及展开收起列表布局的自定义控件
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Releases 1.0.3

#### 效果演示

<img src="img/1.gif"></img>
<img src="img/2.gif"></img>

<img src="img/3.gif"></img>
<img src="img/4.gif"></img>

<img src="img/5.gif"></img>

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:PlaceHolderView:1.0.0')
    ......
 }
  ```
在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

在 layout 文件中:

```xml
<com.mindorks.placeholderview.SwipeDirectionalView
        ohos:id="$+id:swipeView"
        ohos:height="match_parent"
        ohos:width="match_parent"/>
```

在代码中find该组件，并且设置provider和PageSlider。
```
    swd_view = (SwipeDirectionalView) findComponentById(ResourceTable.Id_swipeView);
    swd_view.setSwipeType(SwipeDirectionalView.SWIPE_TYPE_HORIZONTAL);

    for (int i = 0; i < 10; i++) {
        swd_view.addView(makeCard());
    }
```

SwipeDirectionalView添加卡片
```
    swd_view.addView(makeCard());
```

SwipeDirectionalView设置卡片显示个数
```
    swd_view.setDisplayViewCount(3);
```

SwipeDirectionalView设置滑动方向
```
    swd_view.setSwipeType(SwipeDirectionalView.SWIPE_TYPE_HORIZONTAL);
```

SwipeDirectionalView设置卡片缩放比例以及叠加间距
```
    swd_view.setSwipeDecor(new SwipeDecor()
                    .setPaddingTop(10)
                    .setRelativeScale(0.01f));
```

加载更多功能集成使用了三方控件XRecyclerView，加载动画目前只支持文字效果。


#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息

Copyright (C) 2016 Janishar Ali Anwar

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License



